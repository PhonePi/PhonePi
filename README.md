PhonePi is an attempt to build DIY phone with freedom in mind.
The main reason behind this project is fun!
*... and the hope that we can make it*

Main goals are:

* Reproducibility
* Staying open to examination and modification
* Reasonable cost

We would like to see dominance of FOSS tools in phone itself and in a process of development.
For example we are already using:

* [Archlinux ARM](https://archlinuxarm.org/) as phone OS
* [Rust](https://www.rust-lang.org/) as main language for the phone tooling
* [Qt](https://www.qt.io/developers/) as toolkit for GUI
* [Gitlab](https://gitlab.com/) for project management, VCS, CI, wiki
* [KiCad](http://kicad-pcb.org/) for creating PCBs
* [OpenScad](http://www.openscad.org/) for the body

Even thought we love all the stuff written above you can use whatever tool you feel comfortable.
It's also possible than something will change.
For example someone can figure out how to properly use GTK or Qt's JIT on armv6 to use QML.

For now we are planing to use Raspberry Pi Zero (W) as main processing unit.
It's now FOSS complaint but we are trying to minimize use of proprietary binary blobs.
This is one of the reasons we are using vc4 (the other one - it's freaking cool).
Excuse: other alternatives are not as accessible to us or other DIY enthusiasts.
We would love to see RISC-V board in our project.
And we would love to use [Librem 5](https://puri.sm/shop/librem-5/) =]


### Roadmap

* First we want to build the phone out of existing modules by wiring them up in some (maybe horrible) way.
* When we can get it working we can start adapting existing and creating new software.
* After getting all the modules working together we can start creating a board to house most parts of phone logic:
	* Power supply
	* Connectivity module (GSM, HSPA+, LTE or other)
	* Additional buttons and power logic module
	* Other if possible or reasonable
* Creating a 3D printable body
* Full assembly

. . .

* Profit!

### Who we are
We are a team of two developers from Russia and we are open to your contribution.

This is the second iteration of PhonePi (also known as PolyPhone and XXon).
First attempt was a university project of six students.
Not all of them were interested in the project because of what we got some imbalance in software and hardware part.
After failing to deliver hardware we were stuck with a bunch of wires and chips not working together.
System was tested only in partly assembled state.
Now it's dead and we are afraid to touch it.
You can see some of this stuff: [sources (gitlab mirror)](https://gitlab.com/PhonePi/v0), [instagram (up to 2018)](https://www.instagram.com/phone3141592/), [twitter (up to 2018)](https://twitter.com/PhonePi), [original repos (beware of Russian)](https://git.ejiek.com/KSPT)

